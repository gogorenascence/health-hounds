import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);

// export async function loadData() {
//   const response = await fetch("http://localhost:8080/api/thirtyday/");

//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App thirtydayworkouts={data.thirtydayworkouts} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }
// loadData();
