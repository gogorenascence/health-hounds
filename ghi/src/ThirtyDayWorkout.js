import React, { useState, useEffect } from "react";
import axios from "axios";
import "./ThirtyDayWorkout.css";

const ThirtyDayWorkout = () => {
  const [calendarData, setCalendarData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_ACCOUNTS_HOST}/api/thirtyday`
        );
        const data = response.data.thirty_day_workouts;
        const dataArray = Object.values(data);
        setCalendarData(dataArray);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);

  return (
    <>
      <div className="header">
        <h1>This is a 30-day program with 6 days of workouts.</h1>
        <h2>Take a rest day once per week where you see fit.</h2>
        <p>
          For all exercises, we recommend a rep range between 8-15 and 4-5 sets.
        </p>
        <p>
          Keep note of how many reps/sets you perform as well as the weight you
          are using.
        </p>
        <p>
          It is suggested for progress to increase either reps, sets, or weight
          as you repeat the workouts in weeks 2, 3, and 4.
        </p>
      </div>
      <div className="audio-player">
        <iframe
          title="Embedded Spotify Track"
          style={{ borderRadius: "12px" }}
          src="https://open.spotify.com/embed/track/5D8sJEbM2zIAZkWn9wSDXl?utm_source=generator"
          width="100%"
          height="352"
          frameBorder="0"
          allowFullScreen=""
          allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
          loading="lazy"
        ></iframe>
      </div>
      <div className="calendar">
        {calendarData.map((day) => (
          <div className="calendar-day" key={day.id}>
            <h3>Day {day.day_id}</h3>
            <h4>{day.workout_name}</h4>
            <h5>{day.exercise_by_name}</h5>
            <h6>{day.instructions}</h6>
            <ul></ul>
          </div>
        ))}
      </div>
    </>
  );
};

export default ThirtyDayWorkout;
