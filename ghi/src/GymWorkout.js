import "./App.css";
import { useGetTokenQuery } from "./redux/api";
import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const GymWorkout = () => {
  const [count, setCount] = useState(1);
  const [muscles] = useState(["abdominals", "adductors", "biceps", "calves",
  "chest", "forearms", "glutes", "hamstrings", "lats", "neck", "quadriceps",
  "shoulders", "traps", "triceps",
]);
  const [difficulties] = useState(["beginner", "intermediate", "expert"]);
  const [selectedMuscles, setSelectedMuscles] = useState([]);
  const [selectedDifficulty, setSelectedDifficulty] = useState("");
  const { data: token } = useGetTokenQuery();
  const navigate = useNavigate();

  const handleChangeCount = (event) => {
  setCount(event.target.value);
  };

  const handleChangeMuscle = (event) => {
    const { name, checked } = event.target;
    if (checked) {
      setSelectedMuscles((prevSelectedMuscles) => [
        ...prevSelectedMuscles,
        name,
      ]);
    } else {
      setSelectedMuscles((prevSelectedMuscles) =>
        prevSelectedMuscles.filter((muscle) => muscle !== name)
      );
    }
  };

  const handleChangeDifficulty = (event) => {
    setSelectedDifficulty(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const totalMuscles = Math.min(selectedMuscles.length, count);
    const exercisesPerMuscle = Math.floor(count / totalMuscles);
    const remainingExercises = count % totalMuscles;

    let totalResponseData = [];

    for (let i = 0; i < totalMuscles; i++) {
      const muscle = selectedMuscles[i];
      let muscleCount = exercisesPerMuscle;

      if (i < remainingExercises) {
        muscleCount += 1;
    }
      try {
        const response = await axios.get(`${process.env.REACT_APP_ACCOUNTS_HOST}/exercise`, {
          params: {
            muscle: muscle,
            difficulty: selectedDifficulty,
            count: muscleCount
          },
        });

        totalResponseData = totalResponseData.concat(response.data)

      } catch (error) {
        console.error(error);
      }
  }

  const allData = {
    'totalResponseData': totalResponseData,
    'user_id': token.account.id,
    'muscles': selectedMuscles,
    'difficulty': selectedDifficulty,
    'type': 'gym',
  }

  try {
    const backendResponse = await axios.post(
      `${process.env.REACT_APP_ACCOUNTS_HOST}/exercise`,
      allData
    );

    const workoutQuery = backendResponse.data.id;
    navigate(`/gym-display?workout=${workoutQuery}`);

  } catch (error) {
    console.error(error);
  }

  };

  return (
    <div>
      <h1 style={{ paddingTop: "3px", textAlign: "center", paddingBottom: "2px" }}>
        Gym Workout Generator
      </h1>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <form onSubmit={handleSubmit}>
          <div>
            {muscles.map((muscle) => (
              <div key={muscle} style={{ marginBottom: "10px" }}>
                <label htmlFor={muscle}>
                  <input
                    type="checkbox"
                    id={muscle}
                    name={muscle}
                    checked={selectedMuscles.includes(muscle)}
                    onChange={handleChangeMuscle}
                    style={{ marginRight: "5px" }}
                  />
                  {muscle}
                </label>
              </div>
            ))}
          </div>
          <div>
            <p>Difficulty:</p>
            {difficulties.map((difficulty) => (
              <div key={difficulty} style={{ marginBottom: "10px" }}>
                <label htmlFor={difficulty}>
                  <input
                    type="radio"
                    id={difficulty}
                    name="difficulty"
                    value={difficulty}
                    checked={selectedDifficulty === difficulty}
                    onChange={handleChangeDifficulty}
                    style={{ marginRight: "5px" }}
                  />
                  {difficulty}
                </label>
              </div>
            ))}
          </div>
          <div>
            <p>Number of Exercises</p>
            <div>
              <input type="text" value={count} onChange={handleChangeCount} />
            </div>
          </div>
          <button type="submit" style={{ marginTop: "10px" }}>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default GymWorkout;
