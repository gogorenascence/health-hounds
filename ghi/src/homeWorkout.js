import "./App.css";
import { useGetTokenQuery } from "./redux/api";
import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const HomeWorkout = () => {
  const [count, setCount] = useState(1);
  const [types] = useState(["stretching", "plyometrics", "cardio"]);
  const [difficulties] = useState(["beginner", "intermediate", "expert"]);
  const [selectedTypes, setSelectedTypes] = useState([]);
  const [selectedDifficulty, setSelectedDifficulty] = useState("");
  const { data: token } = useGetTokenQuery();
  const navigate = useNavigate();

  const handleChangeCount = (event) => {
  setCount(event.target.value);
  };

  const handleChangeType = (event) => {
    const { name, checked } = event.target;
    if (checked) {
      setSelectedTypes((prevSelectedTypes) => [
        ...prevSelectedTypes,
        name,
      ]);
    } else {
      setSelectedTypes((prevSelectedTypes) =>
        prevSelectedTypes.filter((type) => type !== name)
      );
    }
  };

  const handleChangeDifficulty = (event) => {
    setSelectedDifficulty(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const totalTypes = Math.min(selectedTypes.length, count);
    const exercisesPerType = Math.floor(count / totalTypes);
    const remainingExercises = count % totalTypes;

    let totalResponseData = [];

    for (let i = 0; i < totalTypes; i++) {
      const type = selectedTypes[i];
      let typeCount = exercisesPerType; // Set default count per type

      if (i < remainingExercises) {
        typeCount += 1; // Add remaining exercise to the current type
    }
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_ACCOUNTS_HOST}/exercise`,
          {
            params: {
              type: type,
              difficulty: selectedDifficulty,
              count: typeCount,
            },
          }
        );

        totalResponseData = totalResponseData.concat(response.data)

      } catch (error) {
        console.error(error);
      }
  }

  const allData = {
    'totalResponseData': totalResponseData,
    'user_id': token.account.id,
    'difficulty': selectedDifficulty,
    'types': selectedTypes,
    'type': 'home'
  }

  try {
    const backendResponse = await axios.post(
      `${process.env.REACT_APP_ACCOUNTS_HOST}/exercise`,
      allData
    );
    const workoutQuery = backendResponse.data.id;
    navigate(`/home-display?workout=${workoutQuery}`);

  } catch (error) {
    console.error(error);
  }

  };

  return (
    <div>
      <h1
        style={{ paddingTop: "3px", textAlign: "center", paddingBottom: "2px" }}
      >
        Home Workout Generator
      </h1>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <form onSubmit={handleSubmit}>
          <div>
            {types.map((type) => (
              <div key={type} style={{ marginBottom: "10px" }}>
                <label htmlFor={type}>
                  <input
                    type="checkbox"
                    id={type}
                    name={type}
                    checked={selectedTypes.includes(type)}
                    onChange={handleChangeType}
                    style={{ marginRight: "5px" }}
                  />
                  {type}
                </label>
              </div>
            ))}
          </div>
          <div>
            <p>Difficulty:</p>
            {difficulties.map((difficulty) => (
              <div key={difficulty} style={{ marginBottom: "10px" }}>
                <label htmlFor={difficulty}>
                  <input
                    type="radio"
                    id={difficulty}
                    name="difficulty"
                    value={difficulty}
                    checked={selectedDifficulty === difficulty}
                    onChange={handleChangeDifficulty}
                    style={{ marginRight: "5px" }}
                  />
                  {difficulty}
                </label>
              </div>
            ))}
          </div>
          <div>
            <p>Number of Exercises</p>
            <div>
              <input type="text" value={count} onChange={handleChangeCount} />
            </div>
          </div>
          <button type="submit" style={{ marginTop: "10px" }}>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default HomeWorkout;
