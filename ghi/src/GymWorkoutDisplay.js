import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { Container } from "react-bootstrap";
import axios from "axios";
import { useLocation } from "react-router-dom";
import NameChangeModal from "./WorkoutNameModal";
import { useNavigate } from "react-router-dom";

const GymWorkoutDisplay = () => {
  const [exercises, setExercises] = useState([]);
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const workout = queryParams.get("workout")
  const navigate = useNavigate();

  const handleDelete = (event) => {
    event.preventDefault();

    axios
      .delete(`${process.env.REACT_APP_ACCOUNTS_HOST}/api/workouts/${workout}`)
      .then((response) => {
        console.log("Deletion successful", response);
      })
      .catch((error) => {
        console.error("Error during deletion", error);
      });

    navigate("/gym-workout");
  };

  useEffect(() => {
    const fetchExercises = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_ACCOUNTS_HOST}/api/workouts/${workout}`,
          {
            params: {
              workout,
            },
          }
        );
        const allExercises = response.data.exercises[0];

        setExercises(allExercises)
      } catch (error) {
        console.error(error);
      }
    };

    fetchExercises();
  }, [workout]);

  return (
    <>
      <div>
        <h1>Gym Workout</h1>
        <ul>
          {exercises.map((exercise) => (
            <li key={exercise.name}>
              <h3>{exercise.name}</h3>
              <p>Instructions: {exercise.instructions}</p>
              <p>Muscle: {exercise.muscle}</p>
              <p>Difficulty: {exercise.difficulty}</p>
            </li>
          ))}
        </ul>
      </div>
      <Container padding="20px">
      <div>
        <NameChangeModal workout={workout}/>{' '}
        <Button onClick={handleDelete} >
          Don't Save
        </Button>
      </div>
    </Container>
    </>
  );
};

export default GymWorkoutDisplay;
