import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import Navv from "./Nav";
import GymWorkout from "./GymWorkout";
import HomeWorkout from "./homeWorkout";
import ProfilePage from "./ProfilePage";
import "./App.css";
import { useGetTokenQuery } from "./redux/api";
import LandingPage from "./landingPage";
import GymWorkoutDisplay from "./GymWorkoutDisplay";
import HomeWorkoutDisplay from "./HomeWorkoutDisplay";
import ThirtyDayWorkout from "./ThirtyDayWorkout";
import AboutUsPage from "./AboutUs";
import Thirty_day_workout from "./30dayworkout";


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  const { data: token } = useGetTokenQuery();
  // const dispatch = useDispatch();

  return (
    <BrowserRouter basename={basename}>
      <Navv />
      <main className="container content">
        <Routes>
          <Route path="/" element={token ? <ProfilePage /> : <LandingPage />} />
          <Route path="/" element={<Outlet />} />
          <Route path="/about-us" element={<AboutUsPage />} />
          {token && (
            <>
              <Route path="/gym-workout" element={<GymWorkout />} />
              <Route path="/gym-display" element={<GymWorkoutDisplay />} />
              <Route path="/home-workout" element={<HomeWorkout />} />
              <Route path="/home-display" element={<HomeWorkoutDisplay />} />
              <Route path="/thirtydayworkout" element={<ThirtyDayWorkout />} />
              <Route path="/30dayworkout" element={<Thirty_day_workout />} />
            </>
          )}
        </Routes>
      </main>
    </BrowserRouter>
  );
}

export default App;
