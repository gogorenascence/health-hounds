import React from 'react';
import './AboutUs.css';
import linkedin from './linkedin.png';
import gmail from "./gmail.png";


function AboutUsPage() {
  const teamMembers = [
    {
      name: 'Junjie Gao',
      role: 'Software Engineer',
      linkedin: 'https://www.linkedin.com/in/junjie-gao/',
      email: 'junjie4799@gmail.com',
    },
    {
      name: 'Cape Monn',
      role: 'Software Engineer',
      linkedin: 'https://www.linkedin.com/in/cape-monn/',
      email: 'capebmonn@gmail.com',
    },
    {
      name: 'Benjamin Bass',
      role: 'Software Engineer',
      linkedin: 'https://www.linkedin.com/in/ben-bass8/',
      email: 'benbass8@gmail.com',
    },
    {
      name: 'Aaron Guiang',
      role: 'Software Engineer',
      linkedin: 'https://www.linkedin.com/in/aaronguiang1129/',
      email: 'aaronzguiang@gmail.com',
    },
  ];

  return (
    <div className="about-us-page">
      <header>
        <h1>About Us</h1>
        <p>We're passionate software engineers who created Health Hounds to make fitness more accessible.</p>
      </header>
      <section className="team-members">
        <h2>Meet Our Team</h2>
        <div className="member-list">
          {teamMembers.map((member, index) => (
            <div className="member" key={index}>
              <h3>{member.name}</h3>
              <p>{member.role}</p>
              <div className="contact">
                <div>
                  <a href={member.linkedin} target="_blank" rel="noopener noreferrer">
                    <img src={linkedin} alt="LinkedIn" />
                  </a>
                  <a href={`mailto:${member.email}`}>
                    <img src={gmail} alt="Email" />
                  </a>
                </div>
              </div>
            </div>
          ))}
        </div>
      </section>
      <footer>
        <p>&copy; 2023 Health Hounds. All rights reserved.</p>
      </footer>
    </div>
  );
}

export default AboutUsPage;
