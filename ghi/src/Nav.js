import { useGetTokenQuery, useLogOutMutation } from "./redux/api";
import React from "react";
import { useDispatch } from "react-redux";
import {
  showModalSignUp,
  showModalLogIn,
  LOG_IN_MODAL,
  SIGN_UP_MODAL,
} from "./redux/accountSlice";
import LogInModal from "./LogInModal";
import SignUpModal from "./SignUpModal";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useNavigate } from "react-router-dom";

function Navv() {
  const { data: token } = useGetTokenQuery();
  const dispatch = useDispatch();
  const [logOut] = useLogOutMutation();
  const navigate = useNavigate();

  const returnButton = () => {
    window.location.href = "/";
    logOut();
  };

  return (
    <>
      <Navbar
        collapseOnSelect
        bg="dark"
        expand="lg"
        variant="dark"
        className="shadow p-3"
      >
        <Container>
          <button
            type="button"
            onClick={() => {
              navigate("/");
            }}
          >
            Health Hounds
          </button>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <button
                type="button"
                onClick={() => {
                  navigate("/about-us");
                }}
              >
                About Us
              </button>
              {token ? (
                <>
                  <button
                    type="button"
                    onClick={() => {
                      navigate("/gym-workout");
                    }}
                  >
                    Gym Workouts
                  </button>
                  <button
                    type="button"
                    onClick={() => {
                      navigate("/home-workout");
                    }}
                  >
                    Home Workouts
                  </button>
                  {/* <button type="button" onClick={() => {navigate("/thirtydayworkout")}} >30 Day Program</button> */}
                  <button
                    type="button"
                    onClick={() => {
                      navigate("/30dayworkout");
                    }}
                  >
                    30 Day Workout
                  </button>
                </>
              ) : null}
            </Nav>
            <Nav>
              <div>
                {!token ? (
                  <>
                    <Button
                      onClick={() => dispatch(showModalLogIn(LOG_IN_MODAL))}
                      variant="outline-light"
                    >
                      Log In
                    </Button>{" "}
                    <Button
                      onClick={() => dispatch(showModalSignUp(SIGN_UP_MODAL))}
                      variant="outline-light"
                    >
                      Sign Up
                    </Button>
                  </>
                ) : (
                  <>
                    <Button href="/">Hi, {token.account.username}!</Button>{" "}
                    <Button onClick={returnButton}>Log out</Button>
                  </>
                )}
              </div>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <LogInModal />
      <SignUpModal />
    </>
  );
}

export default Navv;
