import axios from 'axios';


async function getExercisesByMuscleGroup(muscle) {
    try {
        const response = await axios.get(
          `${process.env.REACT_APP_ACCOUNTS_HOST}/exercises`,
          {
            params: {
              muscle: muscle,
            },
          }
        );
    const data = response.data;
    // Handle the received data
    } catch (error) {
    // Handle errors
    }
}
