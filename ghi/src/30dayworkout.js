import React from "react";
import "./ThirtyDayWorkout.css";

function Thirty_day_workout() {
  return (
    <>
      <div className="header">
        <h1>This is a 30-day program with 6 days of workouts.</h1>
        <h2>Take a rest day once per week where you see fit.</h2>
        <p>
          For all exercises, we recommend a rep range between 8-15 and 4-5 sets.
        </p>
        <p>
          Keep note of how many reps/sets you perform as well as the weight you
          are using.
        </p>
        <p>
          It is suggested for progress to increase either reps, sets, or weight
          as you repeat the workouts in weeks 2, 3, and 4.
        </p>
      </div>
      <div className="audio-player">
        <iframe
          title="Embedded Spotify Track"
          style={{ borderRadius: "12px" }}
          src="https://open.spotify.com/embed/track/5D8sJEbM2zIAZkWn9wSDXl?utm_source=generator"
          width="100%"
          height="352"
          allowFullScreen=""
          allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
          loading="lazy"
        ></iframe>
      </div>
      <div className="calendar">
        <div>
          <div className="calendar-day">
            <h1>Monday: Chest and Tri these on for size</h1>

            <table>
              <thead>
                <tr>
                  <th>Exercise</th>
                  <th>Instructions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Dumbell Bench Press</td>
                  <td>
                    Lie down on a flat bench with a dumbbell in each hand
                    resting on top of your thighs. The palms of your hands will
                    be facing each other. Then, using your thighs to help raise
                    the dumbbells up, lift the dumbbells one at a time so that
                    you can hold them in front of you at shoulder width. Once at
                    shoulder width, rotate your wrists forward so that the palms
                    of your hands are facing away from you. The dumbbells should
                    be just to the sides of your chest, with your upper arm and
                    forearm creating a 90 degree angle. Be sure to maintain full
                    control of the dumbbells at all times. This will be your
                    starting position. Then, as you breathe out, use your chest
                    to push the dumbbells up. Lock your arms at the top of the
                    lift and squeeze your chest, hold for a second and then
                    begin coming down slowly
                  </td>
                </tr>
                <tr>
                  <td>Chest Dip</td>
                  <td>
                    For this exercise you will need access to parallel bars. To
                    get yourself into the starting position, hold your body at
                    arms length (arms locked) above the bars. While breathing
                    in, lower yourself slowly with your torso leaning forward
                    around 30 degrees or so and your elbows flared out slightly
                    until you feel a slight stretch in the chest. Once you feel
                    the stretch, use your chest to bring your body back to the
                    starting position as you breathe out
                  </td>
                </tr>
                <tr>
                  <td>Cable V-bar push-down</td>
                  <td>
                    Attach a V-Bar to a high pulley and grab with an overhand
                    grip (palms facing down) at shoulder width. Standing upright
                    with the torso straight and a very small inclination
                    forward, bring the upper arms close to your body and
                    perpendicular to the floor. The forearms should be pointing
                    up towards the pulley as they hold the bar. The thumbs
                    should be higher than the small finger. This is your
                    starting position. Using the triceps, bring the bar down
                    until it touches the front of your thighs and the arms are
                    fully extended perpendicular to the floor. The upper arms
                    should always remain stationary next to your torso and only
                    the forearms should move. Exhale as you perform this
                    movement. After a second hold at the contracted position,
                    bring the V-Bar slowly up to the starting point.
                  </td>
                </tr>
                <tr>
                  <td>EZ-bar skullcrushers</td>
                  <td>
                    Using a close grip, lift the EZ bar and hold it with your
                    elbows in as you lie on the bench. Your arms should be
                    perpendicular to the floor. This will be your starting
                    position. Keeping the upper arms stationary, lower the bar
                    by allowing the elbows to flex. Inhale as you perform this
                    portion of the movement. Pause once the bar is directly
                    above the forehead
                  </td>
                </tr>
                <tr>
                  <td>Cable tricep extension</td>
                  <td>
                    Place a bench sideways in front of a high pulley machine.
                    Hold a straight bar attachment above your head with your
                    hands about 6 inches apart with your palms facing down. Face
                    away from the machine and kneel. Place your head and the
                    back of your upper arms on the bench. Your elbows should be
                    bent with the forearms pointing towards the high pulley.
                    This will be your starting position. While keeping your
                    upper arms close to your head at all times with the elbows
                    in, press the bar out in a semicircular motion until the
                    elbows are locked and your arms are parallel to the floor.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="calendar-day">
            <h1>Tuesday: Tuesday Tone</h1>

            <table>
              <thead>
                <tr>
                  <th>Exercise</th>
                  <th>Instructions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Cocoons</td>
                  <td>
                    Begin by lying on your back on the ground. Your legs should
                    be straight and your arms extended behind your head. This
                    will be your starting position. To perform the movement,
                    tuck the knees toward your chest, rotating your pelvis to
                    lift your glutes from the floor. As you do so, flex the
                    spine, bringing your arms back over your head to perform a
                    simultaneous crunch motion.
                  </td>
                </tr>
                <tr>
                  <td>Rower</td>
                  <td>
                    To begin, seat yourself on the rower. Make sure that your
                    heels are resting comfortably against the base of the foot
                    pedals and that the straps are secured. Select the program
                    that you wish to use, if applicable. Sit up straight and
                    bend forward at the hips. There are three phases of movement
                    when using a rower. The first phase is when you come forward
                    on the rower. Your knees are bent and against your chest.
                    Your upper body is leaning slightly forward while still
                    maintaining good posture. Next, push against the foot pedals
                    and extend your legs while bringing your hands to your upper
                    abdominal area, squeezing your shoulders back as you do so.
                    To avoid straining your back, use primarily your leg and hip
                    muscles.
                  </td>
                </tr>
                <tr>
                  <td>Spider Crawl</td>
                  <td>
                    Begin in a prone position on the floor. Support your weight
                    on your hands and toes, with your feet together and your
                    body straight. Your arms should be bent to 90 degrees. This
                    will be your starting position. Initiate the movement by
                    raising one foot off of the ground. Externally rotate the
                    leg and bring the knee toward your elbow, as far forward as
                    possible. Return this leg to the starting position and
                    repeat on the opposite side.
                  </td>
                </tr>
                <tr>
                  <td>Burpee</td>
                  <td>
                    Begin standing with your legs shoulder-width apart. Place
                    your hands on the floor and kick your legs back so you end
                    up with your stomach and thighs on the floor. Your elbows
                    should be bent. From this position, press up like you are
                    doing a push-up and push your hips up. Jump your feet under
                    your hips and stand. Finish the movement by jumping in the
                    air and bringing your hands over your head.
                  </td>
                </tr>
                <tr>
                  <td>Elbow Plank</td>
                  <td>
                    Get into a prone position on the floor, supporting your
                    weight on your toes and your forearms. Your arms are bent
                    and directly below the shoulder. Keep your body straight at
                    all times, and hold this position as long as possible. To
                    increase difficulty, an arm or leg can be raised.
                  </td>
                </tr>
                <tr>
                  <td>Jumping Rope</td>
                  <td>
                    Hold an end of the rope in each hand. Position the rope
                    behind you on the ground. Raise your arms up and turn the
                    rope over your head bringing it down in front of you. When
                    it reaches the ground, jump over it. Find a good turning
                    pace that can be maintained. Different speeds and techniques
                    can be used to introduce variation. Rope jumping is
                    exciting, challenges your coordination, and requires a lot
                    of energy.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="calendar-day">
            <h1>Wednesday: Back, Back, Back Again</h1>

            <table>
              <thead>
                <tr>
                  <th>Exercise</th>
                  <th>Instructions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Pullups</td>
                  <td>
                    Grab the pull-up bar with the palms facing forward using the
                    prescribed grip. Note on grips: For a wide grip, your hands
                    need to be spaced out at a distance wider than your shoulder
                    width. For a medium grip, your hands need to be spaced out
                    at a distance equal to your shoulder width and for a close
                    grip at a distance smaller than your shoulder width. As you
                    have both arms extended in front of you holding the bar at
                    the chosen grip width, bring your torso back around 30
                    degrees or so while creating a curvature on your lower back
                    and sticking your chest out. This is your starting position.
                    Pull your torso up until the bar touches your upper chest by
                    drawing the shoulders and the upper arms down and back.
                    Exhale as you perform this portion of the movement.
                  </td>
                </tr>
                <tr>
                  <td>Shotgun row</td>
                  <td>
                    Attach a single handle to a low cable. After selecting the
                    correct weight, stand a couple of feet back with a
                    wide-split stance. Your arm should be extended, and your
                    shoulder forward. This will be your starting position.
                    Perform the movement by retracting the shoulder and flexing
                    the elbow. As you pull, supinate the wrist, turning the palm
                    upward as you go.
                  </td>
                </tr>
                <tr>
                  <td>Close-grip pull-down</td>
                  <td>
                    Sit down on a pull-down machine with a V-Bar attached to the
                    top pulley. Adjust the knee pad of the machine to fit your
                    height. These pads will prevent your body from being raised
                    by the resistance attached to the bar. Grab the V-bar with
                    the palms facing each other (a neutral grip). Stick your
                    chest out and lean yourself back slightly (around 30
                    degrees) to better engage the lats. This will be your
                    starting position. Using your lats, pull the bar down as you
                    squeeze your shoulder blades. Continue until your chest
                    nearly touches the V-bar. Exhale as you execute this motion.
                  </td>
                </tr>
                <tr>
                  <td>Wide-grip barbell curl</td>
                  <td>
                    Stand up with your torso upright while holding a barbell at
                    the wide outer handle. The palm of your hands should be
                    facing forward. The elbows should be close to the torso.
                    This will be your starting position. While holding the upper
                    arms stationary, curl the weights forward while contracting
                    the biceps as you breathe out. Tip: Only the forearms should
                    move. Continue the movement until your biceps are fully
                    contracted and the bar is at shoulder level. Hold the
                    contracted position for a second and squeeze the biceps
                    hard. Slowly begin to bring the bar back to starting
                    position as your breathe in. Repeat for the recommended
                    amount of repetitions. Variations: You can also perform this
                    movement using an E-Z bar or E-Z attachment hooked to a low
                    pulley. This variation seems to really provide a good
                    contraction at the top of the movement.
                  </td>
                </tr>
                <tr>
                  <td>Incline Hammer Curls</td>
                  <td>
                    Seat yourself on an incline bench with a dumbbell in each
                    hand. You should pressed firmly against he back with your
                    feet together. Allow the dumbbells to hang straight down at
                    your side, holding them with a neutral grip. This will be
                    your starting position. Initiate the movement by flexing at
                    the elbow, attempting to keep the upper arm stationary.
                    Continue to the top of the movement and pause, then slowly
                    return to the start position
                  </td>
                </tr>
                <tr>
                  <td>Zottman Curl</td>
                  <td>
                    Stand up with your torso upright and a dumbbell in each hand
                    being held at arms length. The elbows should be close to the
                    torso. Make sure the palms of the hands are facing each
                    other. This will be your starting position. While holding
                    the upper arm stationary, curl the weights while contracting
                    the biceps as you breathe out. Only the forearms should
                    move. Your wrist should rotate so that you have a supinated
                    (palms up) grip. Continue the movement until your biceps are
                    fully contracted and the dumbbells are at shoulder level.
                    Hold the contracted position for a second as you squeeze the
                    biceps. Now during the contracted position, rotate your
                    wrist until you now have a pronated (palms facing down) grip
                    with the thumb at a higher position than the pinky. Slowly
                    begin to bring the dumbbells back down using the pronated
                    grip. As the dumbbells close your thighs, start rotating the
                    wrist so that you go back to a neutral (palms facing your
                    body) grip.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="calendar-day">
            <h1>Thursday: Shake & Bake Leg Day</h1>

            <table>
              <thead>
                <tr>
                  <th>Exercise</th>
                  <th>Instructions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Barbell Full Squat</td>
                  <td>
                    This exercise is best performed inside a squat rack for
                    safety purposes. To begin, first set the bar on a rack just
                    above shoulder level. Once the correct height is chosen and
                    the bar is loaded, step under the bar and place the back of
                    your shoulders (slightly below the neck) across it. Hold on
                    to the bar using both arms at each side and lift it off the
                    rack by first pushing with your legs and at the same time
                    straightening your torso. Step away from the rack and
                    position your legs using a shoulder-width medium stance with
                    the toes slightly pointed out. Keep your head up at all
                    times and maintain a straight back. This will be your
                    starting position. Begin to slowly lower the bar by bending
                    the knees and sitting back with your hips as you maintain a
                    straight posture with the head up. Continue down until your
                    hamstrings are on your calves. Inhale as you perform this
                    portion of the movement. Begin to raise the bar as you
                    exhale by pushing the floor with the heel or middle of your
                    foot as you straighten the legs and extend the hips to go
                    back to the starting position. Repeat for the recommended
                    amount of repetitions. This type of squat allows a greater
                    range of motion, and allows the trunk to maintain a more
                    vertical position than other types of squats, due to foot
                    position and the higher bar position
                  </td>
                </tr>
                <tr>
                  <td>Dumbbell Lunges</td>
                  <td>
                    Stand with your torso upright holding two dumbbells in your
                    hands by your sides. This will be your starting position.
                    Step forward with your right leg around 2 feet or so from
                    the foot being left stationary behind and lower your upper
                    body down, while keeping the torso upright and maintaining
                    balance. Inhale as you go down. Note: As in the other
                    exercises, do not allow your knee to go forward beyond your
                    toes as you come down, as this will put undue stress on the
                    knee joint. Make sure that you keep your front shin
                    perpendicular to the ground. Using mainly the heel of your
                    foot, push up and go back to the starting position as you
                    exhale. Repeat the movement for the recommended amount of
                    repetitions and then perform with the left leg. Caution:
                    This is a movement that requires a great deal of balance so
                    if you suffer from balance problems you may wish to either
                    avoid it or just use your own bodyweight while holding on to
                    a fixed object. Definitely never perform with a barbell on
                    your back if you suffer from balance issues. Variations:
                    There are several ways to perform the exercise. One way is
                    to alternate each leg. For instance, do one repetition with
                    the right, then the left, then the right and so on. The
                    other way is to do what I call a static lunge where your
                    starting position is with one of your feet already forward.
                    In this case, you just go up and down from that starting
                    position until you are done with the recommended amount of
                    repetitions. Then you switch legs and do the same. A more
                    challenging version is the walking lunges where you walk
                    across the room but in a lunging fashion. For walking lunges
                    the leg being left back has to be brought forward after the
                    lunging action has happened in order to continue moving
                    ahead. This version is reserved for the most advanced
                    athletes. Lunges can be performed with dumbbells as
                    described above or with a barbell on the back, though the
                    barbell variety is better suited for the advanced athletes
                    who have mastered the exercise and no longer have balance
                    issues.
                  </td>
                </tr>
                <tr>
                  <td>Leg Press</td>
                  <td>
                    Using a leg press machine, sit down on the machine and place
                    your legs on the platform directly in front of you at a
                    medium (shoulder width) foot stance. (Note: For the purposes
                    of this discussion we will use the medium stance described
                    above which targets overall development; however you can
                    choose any of the three stances described in the foot
                    positioning section). Lower the safety bars holding the
                    weighted platform in place and press the platform all the
                    way up until your legs are fully extended in front of you.
                    Tip: Make sure that you do not lock your knees. Your torso
                    and the legs should make a perfect 90-degree angle. This
                    will be your starting position. As you inhale, slowly lower
                    the platform until your upper and lower legs make a
                    90-degree angle. Pushing mainly with the heels of your feet
                    and using the quadriceps go back to the starting position as
                    you exhale. Repeat for the recommended amount of repetitions
                    and ensure to lock the safety pins properly once you are
                    done. You do not want that platform falling on you fully
                    loaded.
                  </td>
                </tr>
                <tr>
                  <td>Leg Extensions</td>
                  <td>
                    For this exercise you will need to use a leg extension
                    machine. First choose your weight and sit on the machine
                    with your legs under the pad (feet pointed forward) and the
                    hands holding the side bars. This will be your starting
                    position. Tip: You will need to adjust the pad so that it
                    falls on top of your lower leg (just above your feet). Also,
                    make sure that your legs form a 90-degree angle between the
                    lower and upper leg. If the angle is less than 90-degrees
                    then that means the knee is over the toes which in turn
                    creates undue stress at the knee joint. If the machine is
                    designed that way, either look for another machine or just
                    make sure that when you start executing the exercise you
                    stop going down once you hit the 90-degree angle. Using your
                    quadriceps, extend your legs to the maximum as you exhale.
                    Ensure that the rest of the body remains stationary on the
                    seat. Pause a second on the contracted position. Slowly
                    lower the weight back to the original position as you
                    inhale, ensuring that you do not go past the 90-degree angle
                    limit. Repeat for the recommended amount of times.
                    Variations: As mentioned in the foot positioning section,
                    you can use various foot positions in order to maximize
                    stimulation of certain thigh areas.
                  </td>
                </tr>
                <tr>
                  <td>Banded Hamstring Curl</td>
                  <td>
                    Secure a band close to the ground and place a bench a couple
                    feet away from it. Seat yourself on the bench and secure the
                    band behind your ankles, beginning with your legs straight.
                    This will be your starting position. Flex the knees,
                    bringing your feet towards the bench. You may need to lean
                    back slightly to keep your feet from striking the floor.
                    Pause at the completion of the movement, and then slowly
                    return to the starting position
                  </td>
                </tr>
                <tr>
                  <td>Calf Press</td>
                  <td>
                    Using a leg press machine, sit down on the machine and place
                    your legs on the platform directly in front of you at a
                    medium (shoulder width) foot stance. Lower the safety bars
                    holding the weighted platform in place and press the
                    platform all the way up until your legs are fully extended
                    in front of you without locking your knees. (Note: In some
                    leg press units you can leave the safety bars on for
                    increased safety. If your leg press unit allows for this,
                    then this is the preferred method of performing the
                    exercise.) Your torso and the legs should make perfect
                    90-degree angle. Now carefully place your toes and balls of
                    your feet on the lower portion of the platform with the
                    heels extending off. Toes should be facing forward, outwards
                    or inwards as described at the beginning of the chapter.
                    This will be your starting position. Press on the platform
                    by raising your heels as you breathe out by extending your
                    ankles as high as possible and flexing your calf. Ensure
                    that the knee is kept stationary at all times. There should
                    be no bending at any time. Hold the contracted position by a
                    second before you start to go back down. Go back slowly to
                    the starting position as you breathe in by lowering your
                    heels as you bend the ankles until calves are stretched.
                    Repeat for the recommended amount of repetitions.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="calendar-day">
            <h1>Friday: Friday Frenzy</h1>

            <table>
              <thead>
                <tr>
                  <th>Exercise</th>
                  <th>Instructions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Military press</td>
                  <td>
                    Start by placing a barbell that is about chest high on a
                    squat rack. Once you have selected the weights, grab the
                    barbell using a pronated (palms facing forward) grip. Make
                    sure to grip the bar wider than shoulder width apart from
                    each other. Slightly bend the knees and place the barbell on
                    your collar bone. Lift the barbell up keeping it lying on
                    your chest.Take a step back and position your feet shoulder
                    width apart from each other. Once you pick up the barbell
                    with the correct grip length, lift the bar up over your head
                    by locking your arms. Hold at about shoulder level and
                    slightly in front of your head. This is your starting
                    position. Lower the bar down to the collarbone slowly as you
                    inhale. Lift the bar back up to the starting position as you
                    exhale. Repeat for the recommended amount of repetitions.
                    Variations: This exercise can also be performed sitting as
                    those with lower back problems are better off performing
                    this seated variety.
                  </td>
                </tr>
                <tr>
                  <td>Front to lateral raise</td>
                  <td>
                    In a standing position, hold a pair of dumbbells at your
                    side. This will be your starting position. Keeping your
                    elbows slightly bent, raise the weights directly in front of
                    you to shoulder height, avoiding any swinging or cheating.
                    At the top of the exercise move the weights out in front of
                    you, keeping your arms extended. Lower the weights with a
                    controlled motion. On the next repetition, raise the weights
                    in front of you to shoulder height before moving the weights
                    laterally to your sides.
                  </td>
                </tr>
                <tr>
                  <td>Kettlebell Press</td>
                  <td>
                    Clean two kettlebells to your shoulders. Clean the
                    kettlebells to your shoulders by extending through the legs
                    and hips as you pull the kettlebells towards your shoulders.
                    Rotate your wrists as you do so. Press one directly overhead
                    by extending through the elbow, turning it so the palm faces
                    forward while holding the other kettlebell stationary .
                    Lower the pressed kettlebell to the starting position and
                    immediately press with your other arm
                  </td>
                </tr>
                <tr>
                  <td>Banded Internal Rotation</td>
                  <td>
                    Choke the band around a post. The band should be at the same
                    height as your elbow. Stand with your right side to the band
                    a couple of feet away. Grasp the end of the band with your
                    right hand, and keep your elbow pressed firmly to your side.
                    We recommend you hold a pad or foam roll in place with your
                    elbow to keep it firmly in position. With your upper arm in
                    position, your elbow should be flexed to 90 degrees with
                    your hand reaching away from your torso. This will be your
                    starting position. Execute the movement by rotating your arm
                    in a forehand motion, keeping your elbow in place. Continue
                    as far as you are able, pause, and then return to the
                    starting position
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="calendar-day">
            <h1>Saturday: Hodge Podge Mirage</h1>

            <table>
              <thead>
                <tr>
                  <th>Exercise</th>
                  <th>Instructions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Barbell shrug</td>
                  <td>
                    Stand up straight with your feet at shoulder width as you
                    hold a barbell with both hands in front of you using a
                    pronated grip (palms facing the thighs). Tip: Your hands
                    should be a little wider than shoulder width apart. You can
                    use wrist wraps for this exercise for a better grip. This
                    will be your starting position. Raise your shoulders up as
                    far as you can go as you breathe out and hold the
                    contraction for a second. Tip: Refrain from trying to lift
                    the barbell by using your biceps. Slowly return to the
                    starting position as you breathe in. Repeat for the
                    recommended amount of repetitions. Variations: You can also
                    rotate your shoulders as you go up, going in a semicircular
                    motion from front to rear. However this version is not good
                    for people with shoulder problems. In addition, this
                    exercise can be performed with the barbell behind the back,
                    with dumbbells by the side, a smith machine or with a shrug
                    machine
                  </td>
                </tr>
                <tr>
                  <td>Neck Ups</td>
                  <td>
                    Lie face down with your whole body straight on a flat bench
                    while holding a weight plate behind your head. Tip: You will
                    need to position yourself so that your shoulders are
                    slightly above the end of a flat bench in order for the
                    upper chest, neck and face to be off the bench. This will be
                    your starting position. While keeping the plate secure on
                    the back of your head slowly lower your head (as in saying
                    yes) as you breathe in. Raise your head back up to the
                    starting position in a semi-circular motion as you breathe
                    out. Hold the contraction for a second. Repeat for the
                    recommended amount of repetitions
                  </td>
                </tr>
                <tr>
                  <td>Landmine Twist</td>
                  <td>
                    Position a bar into a landmine or securely anchor it in a
                    corner. Load the bar to an appropriate weight. Raise the bar
                    from the floor, taking it to shoulder height with both hands
                    with your arms extended in front of you. Adopt a wide
                    stance. This will be your starting position. Perform the
                    movement by rotating the trunk and hips as you swing the
                    weight all the way down to one side. Keep your arms extended
                    throughout the exercise. Reverse the motion to swing the
                    weight all the way to the opposite side. Continue
                    alternating the movement until the set is complete
                  </td>
                </tr>
                <tr>
                  <td>Bottoms Up</td>
                  <td>
                    Begin by lying on your back on the ground. Your legs should
                    be straight and your arms at your side. This will be your
                    starting position. To perform the movement, tuck the knees
                    toward your chest by flexing the hips and knees. Following
                    this, extend your legs directly above you so that they are
                    perpendicular to the ground. Rotate and elevate your pelvis
                    to raise your glutes from the floor. After a brief pause,
                    return to the starting position
                  </td>
                </tr>
                <tr>
                  <td>Stairmaster</td>
                  <td>
                    To begin, step onto the stairmaster and select the desired
                    option from the menu. You can choose a manual setting, or
                    you can select a program to run. Typically, you can enter
                    your age and weight to estimate the amount of calories
                    burned during exercise. Pump your legs up and down in an
                    established rhythm, driving the pedals down but not all the
                    way to the floor. It is recommended that you maintain your
                    grip on the handles so that you don't fall. The handles can
                    be used to monitor your heart rate to help you stay at an
                    appropriate intensity. Stairmasters offer convenience,
                    cardiovascular benefits, and usually have less impact than
                    running outside. They are typically much harder than other
                    cardio equipment
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}

export default Thirty_day_workout;
