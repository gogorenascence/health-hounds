import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSignUpMutation } from './redux/api';
import { preventDefault } from './redux/utils';
import { showModalSignUp, updateFieldSignUp} from './redux/accountSlice';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


function SignUpModal() {
  const dispatch = useDispatch();
  const { show, username, password, email } = useSelector(state => state.account.signUp);
  const [signUp] = useSignUpMutation();
  const field = useCallback(
    e => dispatch(updateFieldSignUp({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  return (
    <Modal show={show}>
      <Form onSubmit={preventDefault(signUp, () => ({ username, password, email }))}>
        <Modal.Header closeButton onClick={() => dispatch(showModalSignUp(null))}>
          <Modal.Title>Join the Pack!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control required onChange={field} value={username} name="username" type="text" placeholder="username" />
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control required onChange={field} value={password} name="password" type="password" placeholder="enter password" />
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control required onChange={field} value={email} name="email" type="email" placeholder="enter email" />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" variant="primary" >
            Sign Up
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}

export default SignUpModal;
