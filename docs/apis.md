# APIs

## Account

- **Method**: `POST`, `GET`, `GET`, `PUT`, `DELETE`
- **Path**: `/api/accounts`, `/api/accounts/{account_id}`

Input:

```json
{
  "email": string,
  "password": string,
  "username": string,
}
```

Output:

```json
{
  "email": string,
  "password": string,
  "username": string,
}
```

The Accounts API will create, update, or delete an account for a user on the Health Hounds website. Users will need to enter in all of the information listed to create an account. They will then be able to access the features of the site.

## Token

- **Method**: `GET`
- **Path**: `/api/token`

Input:

```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": "integer",
    "email": "string",
    "username": "string",
    "hashed_password": "string"
  }
}
```

Output:

```json
{
  "access_token": "string",
  "token_type": "Bearer"
}
```

This token is used to authenticate users by associating themselves with the user's account.

## Exercises

- **Method**: `GET`, `POST`
- **Path**: `/api/exercise/`,

Input:

```json
{
  "muscle": "string",
  "difficulty": "string",
  "type": "string",
  "name": "string",
  "count": "integer"
}
```

Output:

```json
{
  "name": "string",
  "type": "string",
  "muscle": "string",
  "equipment": "string",
  "difficulty": "string",
  "instructions": "string"
}
```

The Exercise API will execute a third party API call to `rapidapi.com/apininjas/api/exercises-by-api-ninjas` to grab the exercises. Each individual exercise is grouped into a collection that we call a workout which has its own api endpoint.

## Thirty Day Workout

- **Method**: `GET`
- **Path**: `/api/thirtyday`

Input:

```json
{
  "day_id": "integer",
  "workout_name": "string",
  "exercise_by_name": "string",
  "instructions": "string",
  "id": "integer"
}
```

Output:

```json
{
  "day_id": "integer",
  "workout_name": "string",
  "exercise_by_name": "string",
  "instructions": "string",
  "id": "integer"
}
```

The thirty day workout api is used to call an api endpoint that is linked to a postgres database which has 33 rows of exercises that are associated with an id and a day_id. The exercises are displayed on the front-end in an ordered list based on their day_id so users know which exercises to do on which days.

## Workouts

- **Method**: `GET`, `GET`, `GET`, `PUT`, `DELETE`
- **Path**: `/api/workouts`, `/api/workouts/{id}`, `/api/workouts/{workout_name}`

Input:

```json
workouts: [
{
      "user_id": "integer"
      "workout_name": "string",
      "muscles_worked": [
        "string"
      ],
      "difficulty": "string",
      "exercises": [
        "string"
      ],
      "id": "integer"
}
]
```

Output:

```json
workouts: [
{
      "user_id": "integer"
      "workout_name": "string",
      "muscles_worked": [
        "string"
      ],
      "difficulty": "string",
      "exercises": [
        "string"
      ],
      "id": "integer"
}
]
```

The workouts API is used on the front-end in our Gym Workout Display to make a call to get the collection of exercises to display.
