from pydantic import BaseModel
from typing import List
from queries.pool import pool


class ThirtyDayIn(BaseModel):
    day_id: int
    workout_name: str
    exercise_by_name: str
    instructions: str


class ThirtyDayOut(ThirtyDayIn):
    id: int


class ThirtyDayWorkouts(BaseModel):
    thirty_day_workouts: List[ThirtyDayOut]


class ThirtyDayWorkoutQuery:
    def get_all_thirtyday(self) -> ThirtyDayWorkouts:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        SELECT *
                        FROM thirtyday
                        ORDER BY id
                        """
                )

                results = []

                for row in cur.fetchall():
                    record = {}
                    print("record", record)
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)

                return results
