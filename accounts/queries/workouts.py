from pydantic import BaseModel
from typing import Optional, List, Any
from queries.pool import pool
import json


class Error(BaseModel):
    message: str


class WorkoutIn(BaseModel):
    user_id: Optional[int]
    workout_name: Optional[str]
    muscles_worked: Optional[List[str]]
    type: Optional[str]
    difficulty: Optional[str]
    exercises: Optional[List[Any]]


class WorkoutOut(WorkoutIn):
    id: int


class Workouts(BaseModel):
    workouts: List[WorkoutOut]


class WorkoutQueries:
    def get_workouts_by_user_id(self, user_id: int) -> Workouts:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM workouts
                    WHERE user_id = %s
                    ORDER BY id
                    """,
                    [user_id],
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)

                return results

    def get_all_workouts(self) -> Workouts:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM workouts
                    ORDER BY id
                    """
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)

                return results

    def get_workout_by_id(self, id: int) -> WorkoutOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id
                        , user_id
                        , workout_name
                        , muscles_worked
                        , type
                        , difficulty
                        , exercises
                    FROM workouts
                    WHERE id = %s;
                    """,
                    [id],
                )
                record = result.fetchone()
                if record is None:
                    return None
                return WorkoutOut(
                    id=record[0],
                    user_id=record[1],
                    workout_name=record[2],
                    muscles_worked=record[3],
                    type=record[4],
                    difficulty=record[5],
                    exercises=record[6],
                )

    def create_workout(self, workout: WorkoutIn) -> WorkoutOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                exercises_json = json.dumps(workout.exercises)
                result = cur.execute(
                    """
                    INSERT INTO workouts (user_id
                        , workout_name
                        , muscles_worked
                        , type
                        , difficulty
                        , exercises
                        )
                    VALUES (%s, %s, %s, %s, %s, ARRAY[%s]::jsonb[])
                    RETURNING id;
                    """,
                    [
                        workout.user_id,
                        workout.workout_name,
                        workout.muscles_worked,
                        workout.type,
                        workout.difficulty,
                        exercises_json,
                    ],
                )
                id = result.fetchone()[0]
                return WorkoutOut(
                    id=id,
                    user_id=workout.user_id,
                    workout_name=workout.workout_name,
                    muscles_worked=workout.muscles_worked,
                    type=workout.type,
                    difficulty=workout.difficulty,
                    exercises=workout.exercises,
                )

    def update_workout_name(
        self, workout_id, workout: WorkoutIn
    ) -> WorkoutOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    workout.workout_name,
                    workout_id,
                ]
                cur.execute(
                    """
                        UPDATE workouts
                        SET workout_name = %s
                        WHERE id = %s
                        RETURNING id, workout_name
                        """,
                    params,
                )

                row = cur.fetchone()
                if row is not None:
                    return WorkoutOut(
                        id=row[0],
                        user_id=None,
                        workout_name=row[1],
                        muscles_worked=None,
                        type=None,
                        difficulty=None,
                        exercises=[],
                    )
                return None

    def delete_workout(self, workout_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM workouts
                    WHERE id = %s
                    """,
                    [workout_id],
                )
