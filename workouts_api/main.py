from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os

def get_application():
    app = FastAPI()

    app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            os.environ.get("CORS_HOST", "http://localhost:3000")
        ],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    return app


app = get_application()



# router shtuff


# app.include_router(___.router, tags=["___"])
# app.include_router(___.router, tags=["___"])
# app.include_router(___.router, tags=["___"])
# app.include_router(___.router, tags=["___"])


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }
